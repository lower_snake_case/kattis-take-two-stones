fn main() {
    let mut buf: String = String::new();
    let mut names: Vec<String> = Vec::new();

    names.push("Bob".to_string());
    names.push("Alice".to_string());

    std::io::stdin().read_line(&mut buf).unwrap();

    println!(
        "{}",
        names[(buf.trim().parse::<i32>().unwrap() % 2) as usize]
    );
}
